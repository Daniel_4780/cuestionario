package curso.umg.gt.cuestionarioapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etUsuario, etContrasena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etContrasena = (EditText) findViewById(R.id.etContrasena);
    }

    public void ingresar(View view){
        String usuario = etUsuario.getText().toString();
        String contrasena = etContrasena.getText().toString();

        if(usuario.equals("User") && contrasena.equals("123")){
            Intent i = new Intent(this, CuestinoarioActivity.class);
            startActivity(i);
        }
        else{
            Toast notificacionFallida = Toast.makeText(this, "Credenciales invalidas", Toast.LENGTH_SHORT);
            notificacionFallida.show();
        }
    }
}

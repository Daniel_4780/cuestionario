package curso.umg.gt.cuestionarioapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class IngresarCuestionarioActivity extends AppCompatActivity {

    private EditText etUsuario, etEdad, etSemestre;
    private Spinner spCarreras;
    private CheckBox chTrabaja;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_cuestionario);

        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etEdad = (EditText) findViewById(R.id.etEdad);
        etSemestre = (EditText) findViewById(R.id.etSemestre);
        spCarreras = (Spinner) findViewById(R.id.spCarreras);
        chTrabaja = (CheckBox) findViewById(R.id.ckTrabaja);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.carreras,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCarreras.setAdapter(adapter);
    }

    public void CrearCuestinario(View view){
        Cuestionario cuestionario = new Cuestionario();
        cuestionario.setCantidadAnios(Integer.parseInt(etEdad.getText().toString()));
        cuestionario.setNombreUsuario(etUsuario.getText().toString());
        cuestionario.setNumeroSemestre(Integer.parseInt(etSemestre.getText().toString()));
        cuestionario.setTrabaja(chTrabaja.isChecked());

        Toast notificacionExitosa = Toast.makeText(this, "Cuestionario creado", Toast.LENGTH_LONG);
        notificacionExitosa.show();
    }
}

package curso.umg.gt.cuestionarioapp;

/**
 * Created by Daniel-HDZ on 26/07/2017.
 */

public class Cuestionario {
    private String NombreUsuario;
    private int CantidadAnios;
    private int NumeroSemestre;
    private String CarreraSeleccionada;
    private boolean Trabaja;

    public Cuestionario() {
    }

    public Cuestionario(String nombreUsuario, int cantidadAnios, int numeroSemestre, String carreraSeleccionada, boolean trabaja) {
        NombreUsuario = nombreUsuario;
        CantidadAnios = cantidadAnios;
        NumeroSemestre = numeroSemestre;
        CarreraSeleccionada = carreraSeleccionada;
        Trabaja = trabaja;
    }

    public String getNombreUsuario() {
        return NombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        NombreUsuario = nombreUsuario;
    }

    public int getCantidadAnios() {
        return CantidadAnios;
    }

    public void setCantidadAnios(int cantidadAnios) {
        CantidadAnios = cantidadAnios;
    }

    public int getNumeroSemestre() {
        return NumeroSemestre;
    }

    public void setNumeroSemestre(int numeroSemestre) {
        NumeroSemestre = numeroSemestre;
    }

    public String getCarreraSeleccionada() {
        return CarreraSeleccionada;
    }

    public void setCarreraSeleccionada(String carreraSeleccionada) {
        CarreraSeleccionada = carreraSeleccionada;
    }

    public boolean isTrabaja() {
        return Trabaja;
    }

    public void setTrabaja(boolean trabaja) {
        Trabaja = trabaja;
    }
}

package curso.umg.gt.cuestionarioapp;

/**
 * Created by Daniel-HDZ on 26/07/2017.
 */

public class Carrera {
    private String NombreCarrera;
    private int idCarrera;

    public Carrera() {
    }

    public Carrera(String nombreCarrera, int idCarrera) {
        NombreCarrera = nombreCarrera;
        this.idCarrera = idCarrera;
    }

    public String getNombreCarrera() {
        return NombreCarrera;
    }

    public void setNombreCarrera(String nombreCarrera) {
        NombreCarrera = nombreCarrera;
    }

    public int getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(int idCarrera) {
        this.idCarrera = idCarrera;
    }
}

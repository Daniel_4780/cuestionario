package curso.umg.gt.cuestionarioapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class CuestinoarioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuestinoario);
    }

    public void ingresarCuestionario(View view){
        Intent i = new Intent(this, IngresarCuestionarioActivity.class);
        startActivity(i);
    }
}
